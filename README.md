# asciidoctor container

<!-- BADGIE TIME -->

[![brettops container](https://img.shields.io/badge/brettops-container-209cdf?labelColor=162d50)](https://brettops.io)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Container for publishing documentation with `asciidoctor`.

[Download the example PDF](https://brettops.gitlab.io/containers/asciidoctor/example.pdf)
