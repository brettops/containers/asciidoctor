ARG CONTAINER_PROXY
FROM ${CONTAINER_PROXY}alpine:3

# hadolint ignore=DL3018
RUN apk add --no-cache \
        bash \
        ruby \
        ruby-bundler

COPY Gemfile Gemfile.lock /

RUN bundle config set --local system true \
    && bundle install \
    && asciidoctor --version \
    && asciidoctor-pdf --version
