IMAGE ?= asciidoctor

.PHONY: all build dev

all: build dev

build:
	docker build -t $(IMAGE) .

dev:
	docker run --rm -it -v $(shell pwd):/code -w /code $(IMAGE)
